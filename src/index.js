const R = require('ramda')
const nutjs = require('@nut-tree/nut-js')
const {Point, Region, sleep, mouse} = nutjs

const memoizeWithSelf = R.memoizeWith(R.identity)

const utils = {
  arrayCycleForwards: (array) => array.push(array.shift()),
  arrayCycleBackwards: (array) => array.unshift(array.pop()),
  sleepMs: (ms) => new Promise((resolve) => setTimeout(resolve, ms)),
  isNotNil: R.complement(R.isNil),

  relativeFractionPositionToAbsolutePoint: memoizeWithSelf((fractionPosition, containerRegion) => {
    const [x, y]  = fractionPosition
    const {top, left, height, width} = containerRegion

    const absoluteX = left + parseInt(width * x)
    const absoluteY = top + parseInt(height * y)

    return new Point(absoluteX, absoluteY)
  }),

  absolutePositionToRelativeFractionPosition: memoizeWithSelf((absolutePosition, containerRegion) => {
    const [x, y] = absolutePosition
    const {top, left, height, width} = containerRegion

    const relativeX = x - left
    const relativeY = y - top

    const relativeFractionX = (relativeX / width).toFixed(3)
    const relativeFractionY = (relativeY / height).toFixed(3)

    return [relativeFractionX, relativeFractionY]
  }),

}

utils.relativeFractionRegionCornersToAbsoluteRegion = memoizeWithSelf((fractionRegionCorners, containerRegion) => {
  const [topLeftFractionPosition, bottomRightFractionPosition] = fractionRegionCorners
  const topLeftAbsolutePoint = utils.relativeFractionPositionToAbsolutePoint(topLeftFractionPosition, containerRegion)
  const bottomRightAbsolutePoint = utils.relativeFractionPositionToAbsolutePoint(bottomRightFractionPosition, containerRegion)

  return new Region(
    topLeftAbsolutePoint.x,
    topLeftAbsolutePoint.y,
    (bottomRightAbsolutePoint.x - topLeftAbsolutePoint.x),
    (bottomRightAbsolutePoint.y - topLeftAbsolutePoint.y)
  )
})

utils.executeEventSequence = async (events=[], logFn=(() => {})) => {
  for (const [point=undefined, shouldClick=false, postDelayMs=500, log='.'] of events) {
    logFn(log)
    if (point != undefined) await mouse.setPosition(point)
    if (shouldClick == true) await mouse.leftClick()
    await sleep(postDelayMs)
  }
}

module.exports = utils
